<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define( 'DB_NAME', 'bd_posgrado' );

/** Tu nombre de usuario de MySQL */
define( 'DB_USER', 'root' );

/** Tu contraseña de MySQL */
define( 'DB_PASSWORD', '' );

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define( 'DB_HOST', 'localhost' );

/** Codificación de caracteres para la base de datos. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', '.>4=f-eci432ot/oZhjaHx}H/_E+DDGG@a~.w0l;k!Uv0BRk](DLR`C>d)eL8#e.' );
define( 'SECURE_AUTH_KEY', '5*AiRcNLQD&>SedY.W)CBL204j#}%&a0FxE5/K]/=wz6O|qi}ay$j<5l6{Apv3$!' );
define( 'LOGGED_IN_KEY', '1d&`<wHN&z_cCZOi]hDmqIY(~|b*m2aOT5QLb]$F3>$DMDxbC!J6/g^8}BDqDX%4' );
define( 'NONCE_KEY', 'f:q?gV3t*W3fnN!l%06&f<j|AAq0H-5dM~Od,swF@_+&R8SrNjHAMh;Z}>;p0jt<' );
define( 'AUTH_SALT', 'e/F,=Hl~^/4-y>bKkoFjyWorX1>:k{[eE3K;PK G^j8n.s*GQY#;yC&L(EKEYf1u' );
define( 'SECURE_AUTH_SALT', '7/[@(.5?3opbv!60`@glmt.;v|922hTIK(# X({y!HOX<Hd2o1JyYN4t6C|?G:p8' );
define( 'LOGGED_IN_SALT', 'fzvVm.<CzIp|)~j+QCABIF)>Z?)S6w#XxWN@6cEf,@ZW&l0dN4N>MQPJUxmq6Zho' );
define( 'NONCE_SALT', ']GT(oz{KSP)3;6|n5*[v4bCqy0P=]rTw)U)gzAt7{k]w_P%7Hby#+0sMBcaUF$_>' );

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

